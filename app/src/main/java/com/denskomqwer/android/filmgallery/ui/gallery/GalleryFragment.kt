package com.denskomqwer.android.filmgallery.ui.gallery

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import com.denskomqwer.android.filmgallery.R
import com.denskomqwer.android.filmgallery.data.Film
import com.denskomqwer.android.filmgallery.databinding.FragmentGalleryBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GalleryFragment : Fragment(R.layout.fragment_gallery), FilmAdapter.OnItemClickListener {

    private val viewModel by viewModels<GalleryViewModel>()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentGalleryBinding.bind(view)

        val adapter = FilmAdapter(this)

        binding.apply {
            recyclerView.setHasFixedSize(true)
            recyclerView.adapter = adapter.withLoadStateHeaderAndFooter(
                header = FilmLoadStateAdapter { adapter.retry() },
                footer = FilmLoadStateAdapter { adapter.retry() },
            )
            retryButton.setOnClickListener {
                adapter.retry()
            }
        }

        viewModel.films.observe(viewLifecycleOwner) {
            adapter.submitData(viewLifecycleOwner.lifecycle, it)
        }

        adapter.addLoadStateListener { loadState ->
            binding.apply {
                progressBar.isVisible = loadState.source.refresh is LoadState.Loading
                recyclerView.isVisible = loadState.source.refresh is LoadState.NotLoading
                retryButton.isVisible = loadState.source.refresh is LoadState.Error
                loadErrorTextView.isVisible = loadState.source.refresh is LoadState.Error
            }
        }
    }

    override fun onItemClick(film: Film) {
        val action = GalleryFragmentDirections.actionGalleryFragmentToDescriptionFragment(film,
            film.display_title.ifEmpty { "Film" })
        findNavController().navigate(action)
    }

}