package com.denskomqwer.android.filmgallery.ui.user_reviews

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.denskomqwer.android.filmgallery.data.UserReview
import com.denskomqwer.android.filmgallery.databinding.ItemUserReviewsBinding


class UserReviewAdapter(private val listener: OnItemClickListener) :
    ListAdapter<UserReview, UserReviewAdapter.UserReviewViewHolder>(REVIEW_COMPARATOR){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserReviewViewHolder {
        val binding = ItemUserReviewsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return UserReviewViewHolder(binding)
    }

    override fun onBindViewHolder(holder: UserReviewViewHolder, position: Int) {
        val currentItem = getItem(position)

        if (currentItem != null) {
            holder.bind(currentItem)
        }
    }

    inner class UserReviewViewHolder(private val binding: ItemUserReviewsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val item = getItem(position)
                    if (item != null) {
                        listener.onItemClick(item)
                    }
                }
            }
        }

        fun bind(review: UserReview) {
            binding.apply {
                filmTitleTextView.text = review.title
                reviewTextView.text = review.textReview
                filmRatingTextView.text = review.rating.toString()
                dateTextView.text = "Edited: " + review.dateEditedFormatted
            }
        }
    }


    interface OnItemClickListener {
        fun onItemClick(review: UserReview)
    }


    companion object {
        private val REVIEW_COMPARATOR = object : DiffUtil.ItemCallback<UserReview>() {

            override fun areItemsTheSame(oldItem: UserReview, newItem: UserReview): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: UserReview, newItem: UserReview): Boolean = oldItem == newItem
        }
    }
}