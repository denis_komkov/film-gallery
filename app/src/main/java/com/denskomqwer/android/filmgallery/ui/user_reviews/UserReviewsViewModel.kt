package com.denskomqwer.android.filmgallery.ui.user_reviews

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.denskomqwer.android.filmgallery.data.UserReviewDao
import com.denskomqwer.android.filmgallery.util.CREATE_RESULT_OK
import com.denskomqwer.android.filmgallery.util.EDIT_RESULT_OK
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UserReviewsViewModel @Inject constructor(
    private val userReviewDao: UserReviewDao
) : ViewModel() {

    val searchQuery = MutableStateFlow("")

    val sortOrder = MutableStateFlow(SortOrder.BY_DATE)

    private val reviewsFlow = combine(
        searchQuery,
        sortOrder
    ) { query, sortOrder -> Pair(query, sortOrder) }.flatMapLatest { (query, sortOrder) ->
            userReviewDao.getReviews(query, sortOrder)
    }

    val reviews = reviewsFlow.asLiveData()


    private val reviewsEventChannel = Channel<ReviewsEvent>()
    val reviewsEvent = reviewsEventChannel.receiveAsFlow()

    fun onAddEditResult(result: Int) {
        when (result) {
            CREATE_RESULT_OK -> showReviewSavedMessage("Your review saved")
            EDIT_RESULT_OK -> showReviewSavedMessage("Review changes saved")
        }
    }

    private fun showReviewSavedMessage(message: String) = viewModelScope.launch {
        reviewsEventChannel.send(ReviewsEvent.ShowReviewSavedMessage(message))
    }


    sealed class ReviewsEvent {
        data class ShowReviewSavedMessage(val message: String) : ReviewsEvent()
    }
}

enum class SortOrder { BY_TITLE, BY_DATE, BY_RATING }