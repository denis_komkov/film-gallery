package com.denskomqwer.android.filmgallery.ui.user_review

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.denskomqwer.android.filmgallery.data.UserReview
import com.denskomqwer.android.filmgallery.data.UserReviewDao
import com.denskomqwer.android.filmgallery.util.CREATE_RESULT_OK
import com.denskomqwer.android.filmgallery.util.EDIT_RESULT_OK
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AddEditReviewViewModel @Inject constructor(
    private val userReviewDao: UserReviewDao,
    private val state: SavedStateHandle
) : ViewModel() {

    val review = state.get<UserReview>("review")

    var filmTile = state.get<String>("filmTitle") ?: review?.title ?: ""
        set(value) {
            field = value
            state.set("filmTitle", value)
        }

    var filmReview = state.get<String>("filmReview") ?: review?.textReview ?: ""
        set(value) {
            field = value
            state.set("filmReview", value)
        }

    var rating = state.get<Float>("rating") ?: review?.rating ?: 0F
        set(value) {
            field = value
            state.set("rating", value)
        }


    private val addEditReviewEventChannel = Channel<AddEditReviewEvent>()
    val addEditReviewEvent = addEditReviewEventChannel.receiveAsFlow()


    fun onSaveClick() {
        if (filmTile.isBlank()) {
            showInvalidInputMessage("Title of the film can't be empty")
            return
        }

        if (review != null) {
            val updatedReview = review.copy(title = filmTile, textReview = filmReview, rating = rating, dateEdited = System.currentTimeMillis())
            updateReview(updatedReview)
        } else {
            val newReview = UserReview(title = filmTile, textReview = filmReview, rating = rating, dateEdited = System.currentTimeMillis())
            createReview(newReview)
        }
    }

    private fun createReview(review: UserReview) = viewModelScope.launch {
        userReviewDao.insert(review)
        addEditReviewEventChannel.send(AddEditReviewEvent.NavigateBackWithResult(CREATE_RESULT_OK))
    }

    private fun updateReview(review: UserReview) = viewModelScope.launch {
        userReviewDao.update(review)
        addEditReviewEventChannel.send(AddEditReviewEvent.NavigateBackWithResult(EDIT_RESULT_OK))
    }

    private fun showInvalidInputMessage(message: String) = viewModelScope.launch {
        addEditReviewEventChannel.send(AddEditReviewEvent.ShowInvalidInputMessage(message))
    }

    sealed class AddEditReviewEvent {
        data class ShowInvalidInputMessage(val message: String) : AddEditReviewEvent()
        data class NavigateBackWithResult(val result: Int) : AddEditReviewEvent()
    }
}