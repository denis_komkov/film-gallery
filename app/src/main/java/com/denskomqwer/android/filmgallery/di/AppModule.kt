package com.denskomqwer.android.filmgallery.di

import android.app.Application
import androidx.room.Room
import com.denskomqwer.android.filmgallery.api.MovieReviewsApi
import com.denskomqwer.android.filmgallery.data.ReviewDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(MovieReviewsApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideDevelopersApi(retrofit: Retrofit): MovieReviewsApi =
        retrofit.create(MovieReviewsApi::class.java)

    @Provides
    @Singleton
    fun provideDatabase(
        app: Application
    ) = Room.databaseBuilder(app, ReviewDatabase::class.java, "review_database")
        .fallbackToDestructiveMigration()
        .build()

    @Provides
    fun provideUerReviewDao(db: ReviewDatabase) = db.userReviewDao()
}