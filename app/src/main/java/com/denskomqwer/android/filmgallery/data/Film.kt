package com.denskomqwer.android.filmgallery.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Film(
    val display_title: String,
    val summary_short: String,
    val byline: String,
    val multimedia: FilmMultimedia,
    val link: ReviewLink
) : Parcelable

@Parcelize
data class FilmMultimedia(
    val src: String
) : Parcelable

@Parcelize
data class ReviewLink(
    val url: String,
    val suggested_link_text: String
) : Parcelable