package com.denskomqwer.android.filmgallery

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FilmGalleryApplication : Application() {
}