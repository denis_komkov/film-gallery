package com.denskomqwer.android.filmgallery.data

import androidx.room.*
import com.denskomqwer.android.filmgallery.ui.user_reviews.SortOrder
import kotlinx.coroutines.flow.Flow

@Dao
interface UserReviewDao {

    fun getReviews(query: String, sortOrder: SortOrder): Flow<List<UserReview>> =
        when(sortOrder) {
            SortOrder.BY_DATE -> getReviewsSortedByDateEdited(query)
            SortOrder.BY_TITLE -> getReviewsSortedByTitle(query)
            SortOrder.BY_RATING -> getReviewsSortedByRating(query)
        }

    @Query("SELECT * FROM review_table WHERE title LIKE '%' || :searchQuery || '%' ORDER BY title")
    fun getReviewsSortedByTitle(searchQuery: String): Flow<List<UserReview>>

    @Query("SELECT * FROM review_table WHERE title LIKE '%' || :searchQuery || '%' ORDER BY dateEdited DESC")
    fun getReviewsSortedByDateEdited(searchQuery: String): Flow<List<UserReview>>

    @Query("SELECT * FROM review_table WHERE title LIKE '%' || :searchQuery || '%' ORDER BY rating DESC")
    fun getReviewsSortedByRating(searchQuery: String): Flow<List<UserReview>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(review: UserReview)

    @Update
    suspend fun update(review: UserReview)
}