package com.denskomqwer.android.filmgallery.api

import com.denskomqwer.android.filmgallery.data.Film

class MovieReviewsResponse (
    val results: List<Film>
)