package com.denskomqwer.android.filmgallery.ui.description

import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.denskomqwer.android.filmgallery.R
import com.denskomqwer.android.filmgallery.databinding.FragmentDescriptionBinding

class DescriptionFragment : Fragment(R.layout.fragment_description) {

    private val args by navArgs<DescriptionFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentDescriptionBinding.bind(view)

        binding.apply {
            val film = args.film

            Glide.with(this@DescriptionFragment)
                .load(film.multimedia.src)
                .error(R.drawable.ic_no_photography)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        progressBar.isVisible = false
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        progressBar.isVisible = false
                        titleTextView.isVisible = true
                        reviewLinkTextView.isVisible = true
                        summaryTextView.isVisible = true
                        return false
                    }
                })
                .into(imageView)

            titleTextView.text = film.display_title
            summaryTextView.text = film.summary_short

            val uri = Uri.parse(film.link.url)
            val intent = Intent(Intent.ACTION_VIEW, uri)

            reviewLinkTextView.apply {
                text = getString(R.string.review_by_author, film.link.suggested_link_text, film.byline)
                paint.isUnderlineText = true
                setOnClickListener {
                    context.startActivity(intent)
                }
            }
        }
    }
}