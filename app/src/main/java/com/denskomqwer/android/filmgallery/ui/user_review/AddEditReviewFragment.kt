package com.denskomqwer.android.filmgallery.ui.user_review

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.denskomqwer.android.filmgallery.R
import com.denskomqwer.android.filmgallery.databinding.FragmentAddEditReviewBinding
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class AddEditReviewFragment : Fragment(R.layout.fragment_add_edit_review) {

    private val viewModel by viewModels<AddEditReviewViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentAddEditReviewBinding.bind(view)

        binding.apply {
            titleEditText.setText(viewModel.filmTile)
            reviewEditText.setText(viewModel.filmReview)
            filmRatingBar.rating = viewModel.rating

            titleEditText.addTextChangedListener {
                viewModel.filmTile = it.toString()
            }

            reviewEditText.addTextChangedListener {
                viewModel.filmReview = it.toString()
            }

            filmRatingBar.setOnRatingBarChangeListener { _, rating, _ ->
                viewModel.rating = rating
            }

            saveReviewFab.setOnClickListener {
                viewModel.onSaveClick()
            }
        }



        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.addEditReviewEvent.collect { event ->
                when (event) {
                    is AddEditReviewViewModel.AddEditReviewEvent.ShowInvalidInputMessage -> {
                        Snackbar.make(requireView(), event.message, Snackbar.LENGTH_LONG).show()
                    }
                    is AddEditReviewViewModel.AddEditReviewEvent.NavigateBackWithResult -> {
                        setFragmentResult(
                            "add_edit_request",
                            bundleOf("add_edit_result" to event.result)
                        )
                        findNavController().popBackStack()
                    }
                }
            }
        }
    }
}