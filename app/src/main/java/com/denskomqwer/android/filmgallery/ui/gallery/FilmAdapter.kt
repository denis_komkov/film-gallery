package com.denskomqwer.android.filmgallery.ui.gallery

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.denskomqwer.android.filmgallery.R
import com.denskomqwer.android.filmgallery.data.Film
import com.denskomqwer.android.filmgallery.databinding.ItemGalleryBinding

class FilmAdapter(private val listener: OnItemClickListener)
    : PagingDataAdapter<Film, FilmAdapter.FilmViewHolder>(FILM_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilmViewHolder {
        val binding = ItemGalleryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FilmViewHolder(binding)
    }

    override fun onBindViewHolder(holder: FilmViewHolder, position: Int) {
        val currentItem = getItem(position)

        if (currentItem != null) {
            holder.bind(currentItem)
        }
    }


    inner class FilmViewHolder(private val binding: ItemGalleryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                val position = bindingAdapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val item = getItem(position)
                    if (item != null) {
                        listener.onItemClick(item)
                    }
                }
            }
        }

            fun bind(film: Film) {
                binding.apply {
                    Glide.with(itemView)
                        .load(film.multimedia.src)
                        .centerCrop()
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .error(R.drawable.ic_no_photography)
                        .into(imageView)

                    textViewFilmName.text = film.display_title
                }
            }
        }

    interface OnItemClickListener {
        fun onItemClick(film: Film)
    }

    companion object {
        private val FILM_COMPARATOR = object : DiffUtil.ItemCallback<Film>() {

            override fun areItemsTheSame(oldItem: Film, newItem: Film): Boolean =
                oldItem.multimedia.src == newItem.multimedia.src

            override fun areContentsTheSame(oldItem: Film, newItem: Film): Boolean = oldItem == newItem
        }
    }
}