package com.denskomqwer.android.filmgallery.api

import com.denskomqwer.android.filmgallery.BuildConfig
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieReviewsApi {

    companion object {
        const val BASE_URL = "https://api.nytimes.com/"
        const val API_KEY = BuildConfig.API_KEY
    }

    @GET("svc/movies/v2/reviews/all.json?")
    suspend fun getFilms(
        @Query("offset") page: Int, @Query("api-key") key: String = API_KEY
    ): MovieReviewsResponse
}