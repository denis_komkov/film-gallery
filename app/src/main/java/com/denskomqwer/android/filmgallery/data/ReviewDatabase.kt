package com.denskomqwer.android.filmgallery.data

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [UserReview::class], version = 1, exportSchema = false)
abstract class ReviewDatabase : RoomDatabase() {

    abstract fun userReviewDao():UserReviewDao
}