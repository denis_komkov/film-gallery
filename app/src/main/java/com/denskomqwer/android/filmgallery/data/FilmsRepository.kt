package com.denskomqwer.android.filmgallery.data

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.liveData
import com.denskomqwer.android.filmgallery.api.MovieReviewsApi
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FilmsRepository @Inject constructor(private val movieReviewsApi: MovieReviewsApi) {

    fun getFilms() =
        Pager(
            config = PagingConfig(
                pageSize = 20,
                maxSize = 60,
                enablePlaceholders = false
            ),
            pagingSourceFactory = { FilmsPagingSource(movieReviewsApi) }
        ).liveData
}