package com.denskomqwer.android.filmgallery.data

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.denskomqwer.android.filmgallery.api.MovieReviewsApi
import retrofit2.HttpException
import java.io.IOException

private const val STARTING_OFFSET = 0
private const val PER_PAGE = 20

class FilmsPagingSource(
    private val movieReviewsApi: MovieReviewsApi
) : PagingSource<Int, Film>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Film> {
        val position = params.key ?: STARTING_OFFSET

        return try {
            val response = movieReviewsApi.getFilms(position)
            val films = response.results
            LoadResult.Page(
                data = films,
                prevKey = if (position == STARTING_OFFSET) null else position - PER_PAGE,
                nextKey = if (films.isEmpty()) null else position + PER_PAGE
            )
        } catch (ex: IOException) {
            LoadResult.Error(ex)
        } catch (ex: HttpException) {
            LoadResult.Error(ex)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Film>): Int? {
        return state.anchorPosition
    }
}