package com.denskomqwer.android.filmgallery.ui.user_reviews

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.denskomqwer.android.filmgallery.R
import com.denskomqwer.android.filmgallery.data.UserReview
import com.denskomqwer.android.filmgallery.databinding.FragmentUserReviewsBinding
import com.denskomqwer.android.filmgallery.util.onQueryTextChanged
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class UserReviewsFragment : Fragment(R.layout.fragment_user_reviews), UserReviewAdapter.OnItemClickListener {

    private val viewModel by viewModels<UserReviewsViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentUserReviewsBinding.bind(view)

        val userReviewAdapter = UserReviewAdapter(this)

        binding.apply {
            userReviewsRecyclerView.apply {
                adapter = userReviewAdapter
                layoutManager = LinearLayoutManager(requireContext())
                setHasFixedSize(true)
            }

            addReviewFab.setOnClickListener {
                val action = UserReviewsFragmentDirections.actionUserReviewsFragmentToUserReviewFragment(null, "New review")
                findNavController().navigate(action)
            }
        }

        viewModel.reviews.observe(viewLifecycleOwner) {
            userReviewAdapter.submitList(it)
        }

        setFragmentResultListener("add_edit_request") { _, bundle ->
            val result = bundle.getInt("add_edit_result")
            viewModel.onAddEditResult(result)
        }


        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.reviewsEvent.collect { event ->
                when (event) {
                    is UserReviewsViewModel.ReviewsEvent.ShowReviewSavedMessage -> {
                        Snackbar.make(requireView(), event.message, Snackbar.LENGTH_LONG).show()
                    }
                }
            }
        }

        setHasOptionsMenu(true)
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_fragment_user_reviews, menu)

        val searchItem = menu.findItem(R.id.action_search)
        val searchView = searchItem.actionView as SearchView

        searchView.onQueryTextChanged {
            viewModel.searchQuery.value = it
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_sort_by_title -> {
                viewModel.sortOrder.value = SortOrder.BY_TITLE
                true
            }
            R.id.action_sort_by_date_edited -> {
                viewModel.sortOrder.value = SortOrder.BY_DATE
                true
            }
            R.id.action_sort_by_rating -> {
                viewModel.sortOrder.value = SortOrder.BY_RATING
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onItemClick(review: UserReview) {
        val action = UserReviewsFragmentDirections.actionUserReviewsFragmentToUserReviewFragment(review,
            review.title.ifEmpty { "Film" })
        findNavController().navigate(action)
    }
}