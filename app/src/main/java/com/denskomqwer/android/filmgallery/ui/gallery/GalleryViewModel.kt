package com.denskomqwer.android.filmgallery.ui.gallery

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.denskomqwer.android.filmgallery.data.FilmsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class GalleryViewModel @Inject constructor(
    repository: FilmsRepository
    ): ViewModel() {

        val films = repository.getFilms().cachedIn(viewModelScope)
}