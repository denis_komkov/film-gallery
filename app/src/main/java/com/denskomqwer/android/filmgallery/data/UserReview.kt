package com.denskomqwer.android.filmgallery.data

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize
import java.text.DateFormat
import java.util.*

@Entity(tableName = "review_table")
@Parcelize
data class UserReview (
    val title: String,
    val textReview: String,
    val rating: Float,
    val dateEdited: Long = System.currentTimeMillis(),
    @PrimaryKey val id: String = UUID.randomUUID().toString()
) : Parcelable {
    val dateEditedFormatted: String
        get() = DateFormat.getDateTimeInstance().format(dateEdited)
}