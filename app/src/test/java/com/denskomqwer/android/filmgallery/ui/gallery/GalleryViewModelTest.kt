package com.denskomqwer.android.filmgallery.ui.gallery
import com.denskomqwer.android.filmgallery.data.FilmsRepository
import io.mockk.mockk
import io.mockk.verify

import org.junit.Test

class GalleryViewModelTest {

    @Test
    fun getFilms_calls_repository_getFilms() {
        val repository = mockk<FilmsRepository>(relaxed = true)

        GalleryViewModel(repository)
        verify { repository.getFilms() }
    }
}